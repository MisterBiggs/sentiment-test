# Sentiment Analysis Test

Ran sentiment analysis on a every sentence of a few books and other long pieces of text trying to see if there were any cool visualizations to be made. Project didn't turn out as well as I thought but the work is done so here is the final product. 

![](2001%20Conspiracy.png)

![](Alice%20in%20Wonderland.png)

![](Moby%20Dick.png)

![](The%20King%20James%20Bible.png)

![](Thomas%20Jefferson.png)

![](Unofficial%20Bush%20Biography.png)

![](War%20and%20Peace.png)